import React, { useEffect, useRef, useState } from "react"

type Props = {
    html: string
}

export function HTMLBox({ html }: Props) {
    const boxRef = useRef<HTMLDivElement | null>(null)
    const [overflowing, setOverflowing] = useState(false)
    const [fullHeight, setFullHeight] = useState(false)

    useEffect(() => {
        const el = boxRef.current
        if (el && el.scrollHeight > el.clientHeight) {
            setOverflowing(true)
        }
    }, [setOverflowing, boxRef])

    return (
        <div>
            <div
                className={`description-preset my-3 overflow-hidden transition-all ${
                    fullHeight ? "max-h-1000" : "md:max-h-45 max-h-64"
                }`}
                ref={boxRef}
                dangerouslySetInnerHTML={{ __html: html }}
            />
            {overflowing && !fullHeight && (
                <div
                    className="cursor-pointer text-right text-amber-700 hover:text-amber-900 hover:underline dark:text-violet-300 dark:hover:text-violet-400"
                    onClick={() => setFullHeight(true)}
                >
                    Read more...
                </div>
            )}

            {fullHeight && (
                <div
                    className="cursor-pointer text-right text-amber-700 hover:text-amber-900 hover:underline dark:text-violet-300 dark:hover:text-violet-400"
                    onClick={() => setFullHeight(false)}
                >
                    Read less...
                </div>
            )}
        </div>
    )
}
