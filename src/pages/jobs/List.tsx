import React from "react"
import { fetchJobs, Job } from "../../lib/api/jobs"
import { useAsyncState } from "../../lib/hooks/useAsyncState"
import { Button } from "../../lib/system/Button"
import { Card } from "../../lib/system/Card"
import { HTMLBox } from "./HTMLBox"

export function List() {
    const [jobs, status, error] = useAsyncState(fetchJobs)

    if (status === "pending") {
        return null
    }

    if (status === "error") {
        console.error(error)
        return (
            <div className="m-8 rounded-lg border-2 border-red-400 bg-gray-100 bg-white p-4 text-red-900 shadow-md">
                There was an error fetching job listings, please try again later
            </div>
        )
    }

    return (
        <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
            {jobs?.map((job) => (
                <Card key={job.id} cover={job.company.cover}>
                    <Info job={job} />
                </Card>
            ))}
        </div>
    )
}

type InfoProps = {
    job: Job
}

function Info({ job }: InfoProps) {
    return (
        <div className="">
            <div className="flex space-x-4 font-quicksand text-lg md:text-xl">
                <img
                    className="h-12 w-12 md:h-24 md:w-24"
                    src={job.company.logo}
                />

                <div>
                    {job.title}
                    <div className="font-sm uppercase">{job.company.name}</div>
                </div>
            </div>

            <HTMLBox html={job.descr} />

            <div className="mt-8 mb-1 flex justify-between">
                <Button cto target="_blank" href={job.urls.apply}>
                    Apply
                </Button>
                <Button target="_blank" href={job.urls.ad}>
                    Original Ad
                </Button>
            </div>

            <img src={job.company.cover} className="invisible" />
        </div>
    )
}
