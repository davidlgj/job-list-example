import React, { useState } from "react"
import { Toggle } from "../lib/system/Toggle"
import { List } from "./jobs/List"

export function Page() {
    const [darkmode, setDarkmode] = useState(
        document.documentElement.classList.contains("dark")
    )

    const toggleMode = () => {
        if (!darkmode) {
            localStorage.setItem("darkmode", "dark")
            document.documentElement.classList.add("dark")
        } else {
            localStorage.setItem("darkmode", "light")
            document.documentElement.classList.remove("dark")
        }
        setDarkmode(!darkmode)
    }

    return (
        <div>
            <header className="my-2 mr-4 flex justify-end">
                <Toggle on={darkmode} onToggle={toggleMode} />
            </header>
            <main className="p-2 font-lora sm:p-12">
                <List />
            </main>
        </div>
    )
}
