import React from "react"

type Props = {
    on: boolean
    onToggle: () => void
}

export function Toggle({ onToggle, on }: Props) {
    return (
        <div
            onClick={onToggle}
            className={` flex h-6 w-12 cursor-pointer items-center rounded-full transition-colors ${
                on
                    ? "bg-violet-300 hover:bg-violet-400"
                    : "bg-gray-200 hover:bg-gray-300"
            }`}
        >
            <div
                className={`flex h-5 w-5 items-center justify-center rounded-full bg-white transition-transform dark:bg-violet-900 dark:fill-slate-100 ${
                    on ? "translate-x-6" : "translate-x-1"
                }`}
            >
                {on ? (
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        width="17"
                        height="17"
                    >
                        <path fill="none" d="M0 0h24v24H0z" />
                        <path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-6.671-5.575A8 8 0 1 0 16.425 5.328a8.997 8.997 0 0 1-2.304 8.793 8.997 8.997 0 0 1-8.792 2.304z" />
                    </svg>
                ) : (
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        width="15"
                        height="15"
                    >
                        <path fill="none" d="M0 0h24v24H0z" />
                        <path d="M12 18a6 6 0 1 1 0-12 6 6 0 0 1 0 12zm0-2a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM11 1h2v3h-2V1zm0 19h2v3h-2v-3zM3.515 4.929l1.414-1.414L7.05 5.636 5.636 7.05 3.515 4.93zM16.95 18.364l1.414-1.414 2.121 2.121-1.414 1.414-2.121-2.121zm2.121-14.85l1.414 1.415-2.121 2.121-1.414-1.414 2.121-2.121zM5.636 16.95l1.414 1.414-2.121 2.121-1.414-1.414 2.121-2.121zM23 11v2h-3v-2h3zM4 11v2H1v-2h3z" />
                    </svg>
                )}
            </div>
        </div>
    )
}
