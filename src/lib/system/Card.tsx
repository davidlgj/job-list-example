import React from "react"

export function Card({
    children,
    cover,
}: {
    children: React.ReactNode
    cover?: string
}) {
    return (
        <div
            style={{ backgroundImage: `url(${cover})` }}
            className="overflow-hidden rounded-lg bg-white bg-contain bg-bottom bg-no-repeat p-4 shadow-md dark:border-2 dark:border-fuchsia-400 dark:bg-slate-900"
        >
            {children}
        </div>
    )
}
