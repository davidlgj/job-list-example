import React, { AnchorHTMLAttributes } from "react"

type Props = {
    cto?: boolean
    children: React.ReactNode
} & AnchorHTMLAttributes<HTMLAnchorElement>

export function Button({ children, className, cto, ...props }: Props) {
    return (
        <a
            {...props}
            className={`full cursor-pointer rounded-full px-4 py-1 font-quicksand font-semibold uppercase text-gray-100 ${
                cto
                    ? "bg-teal-500 hover:bg-teal-600 dark:bg-fuchsia-400 dark:hover:bg-fuchsia-500"
                    : "bg-gray-600 hover:bg-gray-700"
            } ${className ?? ""}`}
        >
            {children}
        </a>
    )
}
