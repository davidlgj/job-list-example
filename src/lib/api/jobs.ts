export type Job = {
    id: number
    benefits: unknown[]
    categories: unknown[]
    company: {
        id: number
        slug: string
        name: string
        name_internal: string
        website: string
        industry: string
        descr: string
        logo: string
        cover: string
    }
    contact: {
        name: string
        email: string
        phone: string
        photo: string
    }
    departments: []
    descr: string
    employment_type: string
    experience: string
    from_date: string
    function: string
    language: string
    layers_1: unknown[]
    layers_2: unknown[]
    layers_3: unknown[]
    layers_4: unknown[]
    layers_5: unknown[]
    linkedInCompanyId: number
    locations: {
        location: {
            text: string
        }
    }[]
    slug: string
    title: string
    to_date: null
    urls: {
        ad: string
        apply: string
    }
    video: {
        content: null | string
        url: string
    }
    internal_reference: null | string
    owner: {
        id: number
        name: string
        email: string
    }
    skills: string
}

export const fetchJobs = async (): Promise<Job[]> => {
    const res = await fetch(
        "https://feed.jobylon.com/feeds/7d7e6fd12c614aa5af3624b06f7a74b8/?format=json"
    )
    return (await res.json()) as Job[]
}
