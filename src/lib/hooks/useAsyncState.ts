import { useState, useEffect } from "react"

type State<T> =
    | { status: "pending" }
    | { status: "fetched"; result: T }
    | { status: "error"; error: unknown }

/**
 * Hook to handle async loading, keeping track of lifecycle so
 * we don't get that pesky warning about rendering unmounted component.
 */
export function useAsyncState<TReturn>(
    fetchState: () => Promise<TReturn>,
    dependencies: unknown[] = []
): [null | TReturn, "pending" | "fetched" | "error", null | unknown] {
    const [state, setState] = useState<State<TReturn>>({ status: "pending" })

    useEffect(() => {
        let alive = true
        if (state.status !== "pending") {
            setState({ status: "pending" })
        }
        void fetchState()
            .then((result) => alive && setState({ result, status: "fetched" }))
            .catch((error: unknown) => setState({ status: "error", error }))
        return () => {
            alive = false
        }
    }, dependencies)

    if (state.status === "pending") {
        return [null, "pending", null]
    }
    if (state.status === "error") {
        return [null, "error", state.error]
    }
    return [state.result, state.status, null]
}
