/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: "class",
    content: ["www/index.html", "./src/**/*.tsx"],
    theme: {
        extend: {
            fontFamily: {
                quicksand: ["Quicksand", "sans"],
                lora: ["Lora", "sans-serif"],
            },
        },
    },
    plugins: [],
}
