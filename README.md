# Jobs listing example

## Usage

First install dependencies with npm (or yarn if that's your fancy)

```sh
$ npm install
```

Then start server

```sh
$ npm run serve
```

## Notes on tooling

I went with a setup of `esbuild`, `typescript`, `prettier` and `tailwindcss`. Note that typescript compiler
is only used for typechecking (this pairs well with having typescript LSP in editor of choice) and
esbuild does the actual stripping of types and bundling.

For a full production project, depending on requirements of course, I would have added `postcss` with
`autoprefixer` and let esbuild minify that CSS as well.

Otherwise my philosophy for tooling is KISS. It's important that it's readable and easy to figure
out 6 months later.

## Testing

I didn't write any tests, I find that there is no silver bullit for testing. It all depends on the
project and especially how the backend and other services around looks like. What kind of testing is
most bang for the buck? How important is device support for instance? Is it a SPA app with a lot of
state and business logic in a state handler like Redux, in that case having a lot of coverage on that
instead of the React components usually is better use of time. But if the state and logic is in React
in hooks & contexts then something like `jest` could be very useful.

## Folder structure

This is a small project, but as instructed I structured the folder structure as if it was larger.

Three rule of thumbs:

-   Folders roughly follow URL structure, it should be easy to find where code from looking at the app.
    Placed under the src/pages

-   Local first, i.e. functions and components used togheter are in the same folder. Don't prematurely
    structure out code when its only used in one place just because it looks like it might be useful.
    (I've strayed from this rule here just to make an example structure)
    Don't import from sibling folders or more than about 1 step away up and down.

-   Common code used in more than one place, like common modals, design systems, API services & state
    handling are placed under src/lib
