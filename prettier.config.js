module.exports = {
    plugins: [require('prettier-plugin-tailwindcss')],
    semi: false,
    tabWidth: 4
}
